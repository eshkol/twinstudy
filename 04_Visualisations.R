
### Maintainer: David Novak (david.novak@ugent.be)
### diff_analysis script 04_Visualisations.R
### 2021-12-20

library(tidyverse)

source('99_Inputs.R')
recompute <- FALSE

message('## Differential analysis: 02_Visualisations.R')
message('## ', as.character(Sys.time()))

idx_step <- 1
n_steps <- enable_da + enable_ds_mfis + enable_ds_perc_pos

### Gather group features extracted previously -----

fnames_features <- dir(fpath_features, pattern = '[.]RDS', full.names = TRUE)

features <- lapply(fnames_features, readRDS)

retrieve_features <- function(x, type, force_basename = FALSE) {
  f <- do.call(rbind, lapply(x, function(y) y[[type]]))
  if (force_basename) {
    rownames(f) <- basename(rownames(f))
  }
  f
}

### Gather differential analysis results -----

experiment_info <- readRDS(output_specs['FName_ExperimentInfo'])
idcs_na <- readRDS(output_specs['FName_IdcsNA'])
res_da <- readRDS(output_specs['FName_DifferentialAbundance'])
res_ds_mfis <- readRDS(output_specs['FName_DifferentialStateMFIs'])
res_ds_perc_pos <- readRDS(output_specs['FName_DifferentialStatePercPos'])

## Single-group plot function...
plot_single <- function(d,
                        experiment_info,
                        idcs_na,
                        indep,
                        dep,
                        type,
                        pval) {
  
  name_dep <-
    if (type == 'DA')
      'PercSize'
    else if (type == 'DSMFIs')
      'MFI'
    else if (type == 'DSPercPos')
      'PercentagesPositive'
  
  d <- cbind(d[!idcs_na, dep], experiment_info[, indep])
  
  d <- d[, c(dep, indep)]
  colnames(d)[1] <- name_dep
  
  un <- unique(d[, indep, drop = TRUE])
  bin <- (length(un) < 3) && (0 %in% un || 1 %in% un)
  if (bin) { # binary response variable
    
    d[, indep] <- as.factor(d[, indep, drop = TRUE])
    
    ## Get p-value from Wilcoxon test without multiple-testing correction
    w <- stats::wilcox.test(x = d[d[, indep, drop = TRUE] == 0, name_dep, drop = TRUE],
                            x = d[d[, indep, drop = TRUE] == 1, name_dep, drop = TRUE])
    p <- w$p.value
    
    ggplot(d, aes_string(x = indep, y = name_dep)) +
      scale_fill_manual(values = c('blue', 'red')) +
      geom_violin(width = 0.7, color = NA, alpha = 0.2, aes_string(fill = indep)) +
      geom_boxplot(width = 0.3) +
      xlab(paste0(indep, ' status')) +
      theme(plot.title = element_text(size = 12),
            plot.subtitle = element_text(size = 10),
            legend.position = 'none') +
      labs(title = bquote(bold(.(dep) ~ .(name_dep))),
           subtitle = bquote(atop(bold('Wilcoxon test p: ') ~ .(signif(p, digits = 3)) ~ ', ',
                                  bold('diffcyt adjusted p: ') ~ .(signif(pval, digits = 3)))))
    
  } else { # continuous response variable
    
    ## Get p-value from linear regression without multiple-testing correction
    l <- stats::lm(d[, name_dep, drop = TRUE] ~ d[, indep, drop = TRUE])
    rs <- summary(l)$r.squared
    f <- summary(l)$fstatistic
    p <- stats::pf(f[1], f[2], f[3], lower.tail = FALSE)
    
    ggplot(d, aes_string(x = indep, y = name_dep)) +
      geom_point(col = scales::alpha('#00004d', 0.9), size = 0.2) +
      geom_smooth(se = FALSE, linetype = 'dashed', alpha = 0.8) +
      xlab(indep) +
      ylab(name_dep) +
      ggtitle(paste0(dep, ' ', name_dep)) +
      theme(plot.title = element_text(size = 12),
            plot.subtitle = element_text(size = 10)) +
      labs(title = bquote(bold(.(dep) ~ .(name_dep))),
           subtitle = bquote(atop(bold('LinReg model p: ') ~ .(signif(p, digits = 3)) ~ ', ',
                                  bold('diffcyt adjusted p: ') ~ .(signif(pval, digits = 3)))))
    
  }
}

## All-groups plot function...
plot_all <- function(d,
                     experiment_info,
                     idcs_na,
                     res,
                     type,
                     indep) {
  
  n <- nrow(res)
  
  priority <- if (type == 'DA') 'FDR' else 'adj.P.Val'
  res <- res[order(res[, priority, drop = TRUE], decreasing = TRUE), ]
  
  plot_list <- vector(mode = 'list', length = n)
  for (idx in 1:n) {
    dep <- res$Cluster[idx]
    pval <- res[idx, priority, drop = TRUE]
    plot_list[[idx]] <- plot_single(d = d, experiment_info = experiment_info, idcs_na = idcs_na, indep = indep, dep = dep, type = type, pval = pval)
  }
  
  do.call(cowplot::plot_grid, plot_list)
}

## Volcano plot function
plot_volcano <- function(res,
                         name_col = 'Cluster',
                         change_bound = 0.1,
                         logfold = TRUE,
                         separate_by_markers = FALSE,
                         max_overlaps = 10) {
  
  pval_col <- if ('FDR' %in% colnames(res)) 'FDR' else 'adj.P.Val'
  p_bound <- -log10(0.05)
  
  if (is.null(change_bound)) {
    change_bounds <- NULL
  } else {
    change_bounds <- if (logfold) c(log2(1 + change_bound), -log2(1 + change_bound)) else c(change_bound, -change_bound)
  }
  
  d <- data.frame(Change = if (logfold) res$logFC else sign(res$logFC),
                  p = -log10(res[[pval_col]]),
                  Label = res[[name_col]])
  d$Significant <-
    if (is.null(change_bound)) {
      sapply(1:nrow(d), function(idx) d$p[idx] > p_bound)
    } else {
      sapply(1:nrow(d), function(idx) d$p[idx] > p_bound &&
                                     (d$Change[idx] < change_bounds[2] || d$Change[idx] > change_bounds[1]))
    }
  
  if (separate_by_markers) {
    s <- t(sapply(d$Label, function(x) strsplit(x, ' ')[[1]]))
    colnames(s) <- c('Group', 'Marker')
    rownames(s) <- NULL
    d <- d[, colnames(d) != 'Label']
    d <- cbind(d, s)
  }
  
  p <- ggplot(d, aes(x = Change, y = p)) +
    geom_point(size = 7, alpha = 0.65, colour = 'black') +
    geom_point(size = 6, alpha = 0.65, aes(colour = Significant)) +
    xlim(-max(abs(d$Change)), max(abs(d$Change))) +
    geom_hline(yintercept = p_bound) +
    ggrepel::geom_label_repel(aes(label = .data$Label), alpha = 0.8, force = 10, max.overlaps = max_overlaps) +
    ylab('-log10 p-value') +
    xlab(if (logfold) 'log2 fold-change' else 'change sign') +
    theme_minimal() +
    theme(plot.title = element_text(face = 'bold', size = 16, hjust = 0),
          plot.subtitle = element_text(size = 14, hjust = 0)) +
    labs(title = 'Fold change to p-value',
         subtitle = paste0('+-', change_bound * 100, '% change cutoff'))
  if (!is.null(change_bound)) {
    print('hi')
    print(change_bounds)
    p <- p + geom_vline(xintercept = change_bounds)
  }
  
  if (separate_by_markers) {
    p <- p + facet_wrap(~Marker)
  }
  p
}

### Visualise differential abundance results -----

if (enable_da) {
  message('(', idx_step, '/', n_steps, ') Visualising results of differential abundance analysis')
  idx_step <- idx_step + 1
  message('File name: ', fname_viz_da)
  
  pdf(file = fname_viz_da)
  plot_volcano(res = res_da, change_bound = 0.01)
  plot_all(d = retrieve_features(x = features,
                                 type = if (feature_level == 'clusters') 'cluster_counts' else 'metacluster_counts'),
           experiment_info = experiment_info,
           idcs_na = idcs_na,
           res = res_da,
           type = 'DA',
           indep = target)
  dev.off()
}

if (enable_ds_mfis) {
  message('(', idx_step, '/', n_steps, ') Visualising results of differential state analysis using MFIs')
  idx_step <- idx_step + 1
  message('File name: ', fname_viz_ds_mfis)
  
  pdf(file = fname_viz_mfis)
  plot_volcano(res = res_ds_mfis)
  plot_all(d = retrieve_features(x = features,
                                 type = if (feature_level == 'clusters') 'cluster_mfis' else 'metacluster_mfis'),
           experiment_info = experiment_info,
           idcs_na = idcs_na,
           res = res_ds_mfis,
           type = 'DSMFIs',
           indep = target)
  dev.off()
}

if (enable_ds_perc_pos) {
  message('(', idx_step, '/', n, ') Visualising results of differential state analysis using percentages positive')
  idx_step <- idx_step + 1
  message('File name: ', fname_viz_ds_perc_pos)
  
  pdf(file = fname_viz_perc_pos)
  plot_volcano(res = res_ds_perc_pos)
  plot_all(d = retrieve_features(x = features,
                                 type = if (feature_level == 'clusters') 'cluster_perc_pos' else 'metacluster_perc_pos'),
           experiment_info = experiment_info,
           idcs_na = idcs_na,
           res = res_ds_perc_pos,
           type = 'DSPercPos',
           indep = target)
  dev.off()
}

message('Done')

