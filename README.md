## Cytometry differential analyses

<hr>

This pipeline runs in `R` and uses a number of packages for analysing cytometry data, including `FlowSOM`. `edgeR` and `diffcyt`.

Scripts are run in numeric order by name.
Apart from that, `99_Inputs.R` gives 'hyperparameters' to the analysis that need to be adapted for your data.

Compensation, transformation and batch effect correction is not included in the pipeline at this time.

### Get started

Adapt `99_Inputs.R` to fit your data and analysis hyper-parameters.

### Disclaimer

Be cautious about the differential state analyses using MFIs: fold-change values depend on the distribution of your signal, and as such, having a unified cut-off for fold-change there might not be a good way of determining relevant hits.

The fold changes will be even less informative when looking at percentage positives (stick to looking at the sign of change for now).
I will soon change the change metric to something more sensible.

### Reference

The pipeline relies on functions from `diffcyt`:

Weber, L., Nowicka, M., Soneson, C. and Robinson, M., 2019. diffcyt: Differential discovery in high-dimensional cytometry via high-resolution clustering. Communications Biology, 2(1).

### Maintainer

david.novak@ugent.be
